import dataclasses
import datetime
import json
import pathlib
import subprocess
from os import getenv

from typing import List, Dict, Tuple
from argparse import ArgumentParser

STATE_STORAGE_LOCATION = (
        pathlib.Path(getenv("XDG_DATA_HOME", "~/.local/share")).expanduser()
        / "cron_disk_space.json"
)


def get_usage_percent(directory: pathlib.Path) -> Tuple[float, str, str]:
    df_output = subprocess.Popen(["df", str(directory)], stdout=subprocess.PIPE)
    awk_output = subprocess.Popen(
        ["awk", "{ print $2,$3 }"], stdin=df_output.stdout, stdout=subprocess.PIPE
    )
    result = (
        subprocess.check_output(["tail", "-n", "1"], stdin=awk_output.stdout)
        .decode("utf-8")
        .strip()
        .split(" ")
    )

    if not len(result) == 2:
        raise ValueError(
            f"unable to process output: "
            f"`df {str(directory)}:\n{subprocess.check_output(['df', str(directory)])}")

    total, used = result
    total = int(total)
    used = int(used)
    avail = total - used

    percent = used / total * 100
    percentage_used_str = f"{used / total * 100:.2f}"
    gb_available_str = f"{avail / 1024 / 1024:.2f} GiB"

    return percent, percentage_used_str, gb_available_str


@dataclasses.dataclass
class State:
    last_notification_timestamp: datetime.datetime

    def to_json(self):
        return {
            "last_notification_timestamp": self.last_notification_timestamp.isoformat()
        }

    @staticmethod
    def from_json(d: Dict):
        return State(
            last_notification_timestamp=datetime.datetime.fromisoformat(
                d["last_notification_timestamp"]
            )
        )


def set_state(state: State):
    json_state = state.to_json()

    with STATE_STORAGE_LOCATION.open("w") as f:
        json.dump(json_state, f)


def get_state() -> State:
    if STATE_STORAGE_LOCATION.exists():
        with STATE_STORAGE_LOCATION.open("r") as f:
            state = State.from_json(json.load(f))
    else:
        state = State(last_notification_timestamp=datetime.datetime(1970, 1, 1))

    return state


def send_notification(
        directory: pathlib.Path,
        percentage_used_str: str,
        gb_available_str: str,
        percentage_threshold: float,
        grade_period: int,
        emails: List[str],
):
    body = f"""
Dear users of {str(directory)},

This is an automatic message to notify you that the current disk usage of {str(directory)} is at {percentage_used_str}%, with {gb_available_str} disk space remaining.

Any running jobs are at danger to fail. Please head over to https://mattermost.science.ru.nl/gpu-cluster/channels/town-square to discuss further.

You will be notified again in {grade_period} days if the disk usage has not gone below {percentage_threshold}%.

Kind regards,

{pathlib.Path(__file__).resolve()}
"""

    title = f"{directory} has {gb_available_str} disk space remaining"

    echo_cmd = subprocess.Popen(["echo", body], stdout=subprocess.PIPE)
    command = ["mail", "-s", title, ",".join(emails)]
    subprocess.check_call(command, stdin=echo_cmd.stdout)


def main(
        directory: pathlib.Path,
        notification_percentage: float,
        emails: List[str],
        grace_period: int,
):
    emails = [e.strip() for e in emails]
    state = get_state()

    if (
            state.last_notification_timestamp + datetime.timedelta(days=grace_period)
            > datetime.datetime.now()
    ):
        # we're still in the grace period
        return

    percent, percentage_used_str, gb_available_str = get_usage_percent(directory)

    if percent >= notification_percentage:
        send_notification(
            directory,
            percentage_used_str,
            gb_available_str,
            notification_percentage,
            grace_period,
            emails,
        )
        state.last_notification_timestamp = datetime.datetime.now()
        set_state(state)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "--directory",
        type=pathlib.Path,
        help="the path to the directory to check disk quota for",
        required=True
    )
    parser.add_argument(
        "--notification_percentage",
        type=float,
        help="the threshold percentage (between 0 and 100) triggering a notification",
        required=True
    )
    parser.add_argument(
        "--emails",
        type=str,
        help="a space-separated list of emails to which the notification is send",
        required=True
    )
    parser.add_argument(
        "--grace_period",
        type=int,
        help="time in days before a second notification is send irrespective of"
             " whether the quota is still above the notification threshold",
        required=True
    )

    args = parser.parse_args()

    email_list = args.emails.strip().split(" ")

    main(
        directory=pathlib.Path(args.directory),
        notification_percentage=float(args.notification_percentage),
        emails=email_list,
        grace_period=args.grace_period,
    )
