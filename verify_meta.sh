#!/bin/bash

# directory of script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DIR_FILE=$SCRIPT_DIR/_verify_ceph_das_user_dirs.txt
META_FILE=$SCRIPT_DIR/_verify_ceph_das_meta_files.txt

# find names of all directories
find /ceph/das-scratch/users/ -mindepth 1 -maxdepth 1 -type d -not \( -name 'users' -o -name '.' \) -exec basename {} \; > $DIR_FILE

# find names of all meta files
find /ceph/das-scratch/users/ -maxdepth 1 -type f ! -name '00_Template.meta' -exec sh -c 'echo "$(basename "{}" | sed -e "s/^\.\///;s/\.meta$//")"' \; > $META_FILE

# directories with a matching meta file
echo "User directories with a matching meta file"
cat $DIR_FILE $META_FILE | sort | uniq -d


# directories with a matching meta file
echo "User directories or meta files which were not matched"
cat $DIR_FILE $META_FILE | sort | uniq -u

# print current status
ls -la /ceph/das-scratch/users