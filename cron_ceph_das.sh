#!/usr/bin/env bash
set -e

SCRIPT_DIR=/home/nvaessen/monitor_ceph_das
EMAILS=$("$SCRIPT_DIR"/ceph_das_emails.sh)

/usr/bin/python3 "$SCRIPT_DIR"/cron_disk_space.py \
--directory /ceph/das-scratch/ \
--notification_percentage 99 \
--grace_period 2 \
--emails "$EMAILS"
