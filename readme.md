# Monitor ceph/das-scratch 

This repo contains some scripts to monitor the disk usage of `/ceph/das-scratch` and notify
users when usage goes beyond `95%`.

## set CRON

To enable monitoring and notifications, add `cron_ceph_das.sh` to the crontab:

```bash
$ crontab -e
```

Add the following line:

```bash
0 9,10,18 * * * /path/to/cron_ceph_das.sh
```

This will check the usage every day at 9, 10 and 18 o'clock.

