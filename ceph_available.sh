echo status of /ceph/das-scratch:
for SERVER in cn13 cn79 cn84 cn89 cn99 cn104 cn105 cn114 cn115 cn117; do
  ssh $SERVER 'stat /ceph/das-scratch > /dev/null 2> /dev/null'
  if [ $? -eq 0 ]; then
    printf "\t%s: online\n" $SERVER
  else
    printf  "\t%s: offline\n" $SERVER
  fi
done
